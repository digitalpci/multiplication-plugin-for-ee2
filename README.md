# Multiplication Plugin for EE2 #

This plugin allows you to perform multiplication on two or more EE2 fields.

Example: {exp:multiply numbers="5|2"}
Outputs: 10

Example: {exp:multiply numbers="{field_1}|{field_2}"}
Outputs: The result of multiplying the numeric values in {field_1} and {field_2}